FROM node:10-alpine



RUN apk add --no-cache graphicsmagick

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# RUN npm install
# If you are building your code for production
RUN npm ci --only=production
#-e "NODE_ENV=production"

# Bundle app source
COPY . .
VOLUME "/usr/src/app/public/uploads"
VOLUME "/usr/src/app/cachestorage"

EXPOSE 3000
CMD [ "node", "./bin/www" ]